package com.sanria.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sanria.model.FileDTO;

public class FileDAO {
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost:3306/mydb";
	private String user = "root";
	private String pass = "root";


	public boolean saveFiles(List<FileDTO> fileDTOs){
		Connection conn = null;
		PreparedStatement stmt = null;
		boolean isTrue = false;
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, pass);
			conn.setAutoCommit(false);

			for (FileDTO fileDTO2 : fileDTOs) {
				System.out.println("i am inside foreach");
				String sql = "INSERT INTO filestore VALUES (?,?)";
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, fileDTO2.getFileName());
				stmt.setString(2, fileDTO2.getLocation());
				 stmt.executeUpdate();
				conn.commit();


			}
			} catch (ClassNotFoundException | SQLException e) {
				System.out.println(e.getMessage());
		}
		return isTrue;
		
	}

	public List<String> getFileNames() {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement stmt = null;
		List<String> fileList = new ArrayList<String>();
		try {
			System.out.println("i am inside try block");
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, pass);
			conn.setAutoCommit(false);
			String sql = "Select * From filestore";
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();
			while (rs.next()) {
				fileList.add(rs.getString(1));
			}

			conn.commit();

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println(e.getMessage());
		}

		return fileList;
	}
	
}
