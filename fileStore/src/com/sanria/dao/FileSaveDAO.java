package com.sanria.dao;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileSaveDAO {
	public void saveFile(String uploadDirectory, String filename, String filename2, byte[] bytes) throws IOException {
		BufferedOutputStream stream = new BufferedOutputStream(
				new FileOutputStream(new File(uploadDirectory + File.separator + filename)));
		stream.write(bytes);

		BufferedOutputStream stream2 = new BufferedOutputStream(
				new FileOutputStream(new File(uploadDirectory + File.separator + filename2)));
		stream2.write(bytes);
		stream.flush();
		stream.close();
		stream2.close();

	}

	public void downloadFile(String fileName) {
		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;

		try {
			is = new FileInputStream("E:\\download" + "\\" + fileName);

			isr = new InputStreamReader(is);

			br = new BufferedReader(isr);
			StringBuffer sb = new StringBuffer();
			String line = "";
			String finalLine = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			finalLine = new String(sb);
			// System.out.println(finalLine);

			FileWriter fw = new FileWriter("E:\\output\\" + fileName);
			fw.write(finalLine);
			fw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}
				if (isr != null) {
					try {
						isr.close();
					} catch (IOException e) {
						System.out.println(e.getMessage());
					}
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							System.out.println(e.getMessage());
						}
					}
				}
			}
		}

	}
}
