package com.sanria.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.sanria.dao.FileDAO;
import com.sanria.dao.FileSaveDAO;
import com.sanria.model.DisplayFiles;
import com.sanria.model.FileDTO;

@Controller
public class FileController {
	private static final String UPLOAD_DIRECTORY = "E:\\download";

	@RequestMapping("uploadform")
	public ModelAndView uploadForm() {
		return new ModelAndView("uploadform");
	}

	@RequestMapping(value = "savefile", method = RequestMethod.POST)
	public ModelAndView saveimage(@RequestParam CommonsMultipartFile file, @RequestParam CommonsMultipartFile file2,
			HttpSession session) throws Exception {

		String filename = file.getOriginalFilename();
		String filename2 = file2.getOriginalFilename();
		System.out.println(UPLOAD_DIRECTORY + " " + filename);
		List<FileDTO> fileDTOs = new ArrayList<>();
		FileDTO dto = new FileDTO();
		dto.setFileName(filename);
		dto.setLocation(UPLOAD_DIRECTORY + File.separator + filename);
		fileDTOs.add(dto);

		FileDTO dto2 = new FileDTO();
		dto2.setFileName(filename2);
		dto2.setLocation(UPLOAD_DIRECTORY + File.separator + filename2);
		fileDTOs.add(dto2);
		new FileDAO().saveFiles(fileDTOs);
		byte[] bytes = file.getBytes();

		new FileSaveDAO().saveFile(UPLOAD_DIRECTORY, filename, filename2, bytes);
		return new ModelAndView("uploadform", "filesuccess", "File successfully saved!");

	}

	@RequestMapping("/getFile")
	public String getDownloadPage(Map<String, Object> model, HttpSession session, HttpServletRequest request) {
		DisplayFiles image = new DisplayFiles();
		model.put("userForm", image);

		session = request.getSession(false);

		List<String> imgList = new FileDAO().getFileNames();

		model.put("imgList", imgList);

		return "download";

	}

	@RequestMapping(value = "downloadFile")
	public void downloadFile(HttpServletRequest request, HttpServletResponse response, DisplayFiles image)
			throws IOException {

		ServletOutputStream out;
		out = response.getOutputStream();

		String fileName = image.getFileName();
		new FileSaveDAO().downloadFile(fileName);
		out.print("success");

	}

}
