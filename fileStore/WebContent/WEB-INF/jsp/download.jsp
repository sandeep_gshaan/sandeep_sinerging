<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Load Image</title>
</head>
<body>
    <div align="center">
    	<a href="uploadform"> upload File</a>|
		
	<hr>
        <form:form action="downloadFile"  commandName="userForm">
            <table border="0">
                <tr>
                    <td>FileName:</td>
                    <td><form:select path="fileName" items="${imgList}" /></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="download" /></td>
                </tr>
            </table>
        </form:form>
    </div>
</body>
</html>